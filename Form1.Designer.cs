﻿namespace Camurl
{
     partial class Form1
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
          private System.ComponentModel.IContainer components = null;

          /// <summary>
          /// Clean up any resources being used.
          /// </summary>
          /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
          protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
               this.brandCbx = new System.Windows.Forms.ComboBox();
               this.label1 = new System.Windows.Forms.Label();
               this.edtIp = new System.Windows.Forms.TextBox();
               this.label2 = new System.Windows.Forms.Label();
               this.label3 = new System.Windows.Forms.Label();
               this.edtChannel = new System.Windows.Forms.TextBox();
               this.label4 = new System.Windows.Forms.Label();
               this.edtLogin = new System.Windows.Forms.TextBox();
               this.label5 = new System.Windows.Forms.Label();
               this.edtPwd = new System.Windows.Forms.TextBox();
               this.resultBox = new System.Windows.Forms.TextBox();
               this.findBtn = new System.Windows.Forms.Button();
               this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
               this.tableLayoutPanel2.SuspendLayout();
               this.SuspendLayout();
               // 
               // brandCbx
               // 
               this.brandCbx.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
               this.brandCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
               this.brandCbx.FormattingEnabled = true;
               this.brandCbx.Location = new System.Drawing.Point(135, 13);
               this.brandCbx.Name = "brandCbx";
               this.brandCbx.Size = new System.Drawing.Size(690, 21);
               this.brandCbx.TabIndex = 0;
               // 
               // label1
               // 
               this.label1.AutoSize = true;
               this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
               this.label1.Location = new System.Drawing.Point(13, 10);
               this.label1.Name = "label1";
               this.label1.Size = new System.Drawing.Size(116, 27);
               this.label1.TabIndex = 1;
               this.label1.Text = "Model";
               this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
               // 
               // edtIp
               // 
               this.edtIp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
               this.edtIp.Location = new System.Drawing.Point(135, 40);
               this.edtIp.Name = "edtIp";
               this.edtIp.Size = new System.Drawing.Size(690, 20);
               this.edtIp.TabIndex = 2;
               // 
               // label2
               // 
               this.label2.AutoSize = true;
               this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
               this.label2.Location = new System.Drawing.Point(13, 37);
               this.label2.Name = "label2";
               this.label2.Size = new System.Drawing.Size(116, 26);
               this.label2.TabIndex = 1;
               this.label2.Text = "IP";
               this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
               // 
               // label3
               // 
               this.label3.AutoSize = true;
               this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
               this.label3.Location = new System.Drawing.Point(13, 63);
               this.label3.Name = "label3";
               this.label3.Size = new System.Drawing.Size(116, 26);
               this.label3.TabIndex = 1;
               this.label3.Text = "Channel";
               this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
               // 
               // edtChannel
               // 
               this.edtChannel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
               this.edtChannel.Location = new System.Drawing.Point(135, 66);
               this.edtChannel.Name = "edtChannel";
               this.edtChannel.Size = new System.Drawing.Size(690, 20);
               this.edtChannel.TabIndex = 2;
               // 
               // label4
               // 
               this.label4.AutoSize = true;
               this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
               this.label4.Location = new System.Drawing.Point(13, 89);
               this.label4.Name = "label4";
               this.label4.Size = new System.Drawing.Size(116, 26);
               this.label4.TabIndex = 1;
               this.label4.Text = "Login";
               this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
               // 
               // edtLogin
               // 
               this.edtLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
               this.edtLogin.Location = new System.Drawing.Point(135, 92);
               this.edtLogin.Name = "edtLogin";
               this.edtLogin.Size = new System.Drawing.Size(690, 20);
               this.edtLogin.TabIndex = 2;
               // 
               // label5
               // 
               this.label5.AutoSize = true;
               this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
               this.label5.Location = new System.Drawing.Point(13, 115);
               this.label5.Name = "label5";
               this.label5.Size = new System.Drawing.Size(116, 26);
               this.label5.TabIndex = 1;
               this.label5.Text = "Password";
               this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
               // 
               // edtPwd
               // 
               this.edtPwd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
               this.edtPwd.Location = new System.Drawing.Point(135, 118);
               this.edtPwd.Name = "edtPwd";
               this.edtPwd.Size = new System.Drawing.Size(690, 20);
               this.edtPwd.TabIndex = 2;
               // 
               // resultBox
               // 
               this.tableLayoutPanel2.SetColumnSpan(this.resultBox, 2);
               this.resultBox.Dock = System.Windows.Forms.DockStyle.Fill;
               this.resultBox.Location = new System.Drawing.Point(13, 173);
               this.resultBox.Multiline = true;
               this.resultBox.Name = "resultBox";
               this.resultBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
               this.resultBox.Size = new System.Drawing.Size(812, 346);
               this.resultBox.TabIndex = 3;
               // 
               // findBtn
               // 
               this.findBtn.Location = new System.Drawing.Point(135, 144);
               this.findBtn.Name = "findBtn";
               this.findBtn.Size = new System.Drawing.Size(75, 23);
               this.findBtn.TabIndex = 0;
               this.findBtn.Text = "Find";
               this.findBtn.UseVisualStyleBackColor = true;
               this.findBtn.Click += new System.EventHandler(this.findBtn_Click);
               // 
               // tableLayoutPanel2
               // 
               this.tableLayoutPanel2.AutoSize = true;
               this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
               this.tableLayoutPanel2.ColumnCount = 2;
               this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
               this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
               this.tableLayoutPanel2.Controls.Add(this.edtPwd, 1, 4);
               this.tableLayoutPanel2.Controls.Add(this.brandCbx, 1, 0);
               this.tableLayoutPanel2.Controls.Add(this.edtLogin, 1, 3);
               this.tableLayoutPanel2.Controls.Add(this.findBtn, 1, 5);
               this.tableLayoutPanel2.Controls.Add(this.edtChannel, 1, 2);
               this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
               this.tableLayoutPanel2.Controls.Add(this.edtIp, 1, 1);
               this.tableLayoutPanel2.Controls.Add(this.resultBox, 0, 6);
               this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
               this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
               this.tableLayoutPanel2.Controls.Add(this.label4, 0, 3);
               this.tableLayoutPanel2.Controls.Add(this.label5, 0, 4);
               this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
               this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
               this.tableLayoutPanel2.Name = "tableLayoutPanel2";
               this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(10);
               this.tableLayoutPanel2.RowCount = 7;
               this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
               this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
               this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
               this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
               this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
               this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
               this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
               this.tableLayoutPanel2.Size = new System.Drawing.Size(838, 532);
               this.tableLayoutPanel2.TabIndex = 5;
               // 
               // Form1
               // 
               this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
               this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
               this.ClientSize = new System.Drawing.Size(838, 532);
               this.Controls.Add(this.tableLayoutPanel2);
               this.Name = "Form1";
               this.Text = "Camurl";
               this.Load += new System.EventHandler(this.Form1_Load);
               this.tableLayoutPanel2.ResumeLayout(false);
               this.tableLayoutPanel2.PerformLayout();
               this.ResumeLayout(false);
               this.PerformLayout();

          }

          #endregion

          private System.Windows.Forms.ComboBox brandCbx;
          private System.Windows.Forms.Label label1;
          private System.Windows.Forms.TextBox edtIp;
          private System.Windows.Forms.Label label2;
          private System.Windows.Forms.Label label3;
          private System.Windows.Forms.TextBox edtChannel;
          private System.Windows.Forms.Label label4;
          private System.Windows.Forms.TextBox edtLogin;
          private System.Windows.Forms.Label label5;
          private System.Windows.Forms.TextBox edtPwd;
          private System.Windows.Forms.TextBox resultBox;
          private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
          private System.Windows.Forms.Button findBtn;
     }
}

